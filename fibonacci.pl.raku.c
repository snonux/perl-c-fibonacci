#include <stdio.h>

#define $arg function_argument
#define my int
#define sub int
#define BEGIN int main(void)

my $arg;

sub hello() {
    printf("Hello, welcome to the Fibonacci Numbers!\n");
    printf("This program is all, valid C and C++ and Perl and Raku code!\n");
    printf("It calculates all fibonacci numbers from 0 to 9!\n\n");
    return 0;
}

sub fibonacci() {
    my $n = $arg;

    if ($n < 2) {
        return $n;
    }

    $arg = $n - 1;
    my $fib1 = fibonacci();
    $arg = $n - 2;
    my $fib2 = fibonacci();

    return $fib1 + $fib2;
}

BEGIN {
    hello();
    my $i = 0;

    while ($i <= 10) {
        $arg = $i;
        printf("fib(%d) = %d\n", $i, fibonacci());
        $i++;
    }
}
